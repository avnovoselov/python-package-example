import unittest

from example_pkg.pretty_code import Code


class TestCode(unittest.TestCase):
    def test_constructor(self):
        code_str = '8888'
        code = Code(code_str)

        self.assertEqual(code_str, code.code)
        self.assertEqual('2926a2731f4b312c08982cacf8061eb14bf65c1a87cc5d70e864e079c6220731', code.hash)
