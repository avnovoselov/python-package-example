import math
import re
import unittest

from example_pkg.pretty_code import PrettyCode


class TestPrettyCode(unittest.TestCase):
    def setUp(self) -> None:
        self.__length = 8
        self.__symbols = {'A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f'}
        self.__pretty_code = PrettyCode(self.__length, self.__symbols)

    def test_default_symbols_set(self):
        self.assertEqual(10, len(self.__pretty_code.default_symbol_set))

        for symbol in self.__pretty_code.default_symbol_set:
            self.assertEqual(str, type(symbol))

        for i in range(0, 10):
            self.assertIn(str(i), self.__pretty_code.default_symbol_set)

    def test_constructor(self):
        pretty_code = PrettyCode(4)
        self.assertEqual(list(self.__pretty_code.default_symbol_set), pretty_code.symbols)

    def test_properties(self):
        self.assertEqual(self.__length, self.__pretty_code.length)
        self.assertEqual(list(self.__symbols), self.__pretty_code.symbols)

    def test_random(self):
        length = 4
        code = self.__pretty_code.random(length).code
        self.assertEqual(length, len(code))

        for symbol in code:
            self.assertIn(symbol, self.__symbols)

        code = self.__pretty_code.random().code

        self.assertEqual(self.__pretty_code.length, len(code))

    def test_mirror(self):
        code = self.__pretty_code.mirror().code
        half_length = math.ceil(len(code) / 2)

        for i in range(half_length):
            self.assertEqual(code[i], code[-(i + 1)])

    def test_half_and_random(self):
        code = self.__pretty_code.half_and_random().code
        half_length = math.ceil(self.__pretty_code.length / 2)

        self.assertTrue(re.search(r'(?=(\w))\1{' + str(half_length) + ',}', code))

    def test_sequence(self):
        code = self.__pretty_code.sequence().code

        self.assertEqual(self.__length, len(code))
        self.assertTrue(''.join(sorted(code)) == code or ''.join(sorted(code)) == ''.join(code[::-1]))

        length = 12
        pretty_code = PrettyCode(length)
        code = pretty_code.sequence().code

        self.assertEqual(length, len(code))

    def test_two_sequence(self):
        code = self.__pretty_code.two_sequence().code

        self.assertEqual(self.__length, len(code))

    def test_simplest(self):
        code = self.__pretty_code.simplest().code

        self.assertEqual(1, len(set(code)))
